﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthInfoMB : MonoBehaviour
{
    public string prefix;
    public TextMeshProUGUI text;
    public HeatConsumerMB sensor;
    public Slider slider;

    private void Update()
    {
        SetValue(sensor.heat);
    }

    void SetValue(float value)
    {
        text.text = string.Format("{0}: {1:0.##}", prefix, value);
        slider.value = value;
    }
}
