﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class FancyTooltip : MonoBehaviour
{
    public TextMeshProUGUI tooltip;

    private float timer = 0;
    private float showTime = 3f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                tooltip.text = "";
            }
        }
    }


    public void ShowText(string text)
    {
        tooltip.text = text;

        timer = showTime;
    }
}
