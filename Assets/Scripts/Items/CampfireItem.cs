﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampfireItem : ItemMB
{
    public HeatSourceMB heatSource;
    public GameObject torchPrefab;

    public override void OnInteract(HeroMB hero, ItemMB itemInHand)
    {
        if (itemInHand != null)
        {
            if (itemInHand is LogItem)
            {
                // animation 1 second

                hero.DestroyItemInHands();

                heatSource.AddFuel(30f);
            }
            if (itemInHand is TorchItem)
            {
                // animation 1 second

                hero.DestroyItemInHands();

                var torch = itemInHand.GetHeatSource();
                heatSource.AddFuel(torch.fuel + 0.2f);
            }
            if (itemInHand is SnackItem && heatSource.fuel > 0)
            {
                // animation 3 seconds

                // destroy, feed hero
                hero.DestroyItemInHands();
                hero.SoupBoost();
            }
        } 
        else
        {
            if (heatSource.fuel > 0.1f )
            {
                var torchGO = Instantiate(torchPrefab, transform.position, transform.rotation);

                var torch = torchGO.GetComponentInChildren<HeatSourceMB>();

                torch.fuel = Mathf.Min(heatSource.fuel, torch.fuel);

                heatSource.fuel -= torch.fuel;

                if (heatSource.fuel <= 0)
                {
                    heatSource.fuel = 0.1f; // чтобы спровоцировать затухание.
                }

                hero.PutItemInHands(torchGO.GetComponent<TorchItem>());
            }
        }
        
    }
}
