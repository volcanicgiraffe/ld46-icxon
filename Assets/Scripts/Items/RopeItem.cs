﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeItem : ItemMB
{
    public Transform lowPoint;
    public Transform topPoint;

    public Transform attachPoint;

    public Transform topExitPoint;
    public Transform lowExitPoint;

    private HeroMB attachedHero;

    private float exitDist = 1.0f;

    public override void OnInteract(HeroMB hero, ItemMB itemInHand)
    {
        if (itemInHand == null)
        {
            // animation 1 second

            hero.AttachToRope(this);

            attachedHero = hero;
        }
    }

    private void Update()
    {
        if (attachedHero == null) return;

        if (Vector3.Distance(attachedHero.transform.position, lowPoint.position) < exitDist)
        {
            attachedHero.DeattachFromRope(lowExitPoint);
            attachedHero = null;
            return;
        }

        if (Vector3.Distance(attachedHero.transform.position, topPoint.position) < exitDist)
        {
            attachedHero.DeattachFromRope(topExitPoint);
            attachedHero = null;
            return;
        }
    }
}
