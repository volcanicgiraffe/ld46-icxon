﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMB : MonoBehaviour
{
    public GameObject highlighterPrefab;

    public Collider takeCollider;

    public bool canGrab = false;

    private GameObject highlighter;

    int floorMask;

    private bool disabledHighlighter = false;

    // Start is called before the first frame update
    void Start()
    {
        floorMask = LayerMask.GetMask("Floor");

        highlighter = Instantiate(highlighterPrefab, transform.position, transform.rotation);
        highlighter.transform.SetParent(gameObject.transform);
        highlighter.SetActive(false);

        MoveToFloor();
    }

    public virtual void OnTake()
    {
        transform.localRotation = Quaternion.identity;
        transform.localPosition = Vector3.zero;
        takeCollider.enabled = false;
    }

    public virtual void OnDropped()
    {
        takeCollider.enabled = true;

        MoveToFloor();
    }

    private void MoveToFloor()
    {
        RaycastHit floorHit;

        if (Physics.Raycast(transform.position, -Vector3.up, out floorHit, floorMask))
        {
            transform.localPosition = Vector3.zero;
            transform.position = floorHit.point;
        }
    }

    public virtual void OnPlayerEnter()
    {
        if (disabledHighlighter) return;
        highlighter.SetActive(true);
    }

    public virtual void OnPlayerLeave()
    {
        if (disabledHighlighter) return;
        highlighter.SetActive(false);
    }

    public void DisableHighlighter()
    {
        disabledHighlighter = true;
        highlighter.SetActive(false);
    }

    public virtual void OnInteract(HeroMB hero, ItemMB itemInHand)
    {
        // override?
    }

    // can be null!
    public HeatSourceMB GetHeatSource()
    {
        return GetComponentInChildren<HeatSourceMB>();
    }
}
