﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PondItem : ItemMB
{
    public GameObject fishPrefab;

    private float animationTimer = 10f;
    public Animator fishAnim;

    public override void OnInteract(HeroMB hero, ItemMB itemInHand)
    {
        if (itemInHand != null)
        {
            if (itemInHand is FishtankItem)
            {
                // animation 1 second

                hero.DestroyItemInHands();

                hero.GameCompleted();

                var fish = Instantiate(fishPrefab, Vector3.zero, Quaternion.identity);
                fish.transform.SetParent(this.transform);
                fish.transform.localRotation = Quaternion.identity;
                fish.transform.localPosition = Vector3.zero;

                fish.transform.localScale = new Vector3(15f,15f,15f);

                fishAnim = fish.GetComponent<Animator>();
                fishAnim.SetTrigger("AnimFishRotate");

                DisableHighlighter();
            }
        }
    }

    private void Update()
    {
        if (fishAnim != null)
        {
            if (animationTimer > 0)
            {
                animationTimer -= Time.deltaTime;

                if (animationTimer <= 0)
                {
                    animationTimer = Random.Range(5.0f, 15.0f);
                    fishAnim.SetTrigger("AnimFishRotate");
                }
            }
        }
    }
}
