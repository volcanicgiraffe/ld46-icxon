﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishtankItem : ItemMB
{
    private float animationTimer = 10f;
    public Animator anim;

    public HeatConsumerMB myHeat;

    private void Update()
    {
        if (animationTimer > 0)
        {
            animationTimer -= Time.deltaTime;

            if (animationTimer <= 0)
            {
                animationTimer = Random.Range(5.0f, 15.0f);
                anim.SetTrigger("AnimFishRotate");
            }
        }
        
    }
}
