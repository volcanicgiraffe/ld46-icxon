﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeItem : ItemMB
{
    public GameObject logPrefab;
    public Animator anim;
    public int chopsToDie = 3;

    private bool dead = false;

    public override void OnInteract(HeroMB hero, ItemMB itemInHand)
    {
        if (dead) return;

        if (itemInHand != null && itemInHand is HatchetItem)
        {
            // todo: run animation for 3 seconds

            var log = Instantiate(logPrefab, hero.transform.position, Quaternion.Euler(0, Random.Range(0, 360), 0));

            anim.SetTrigger("AnimTreeChop");
            chopsToDie -= 1;
        }

        if (chopsToDie <= 0)
        {
            dead = true;
            anim.SetTrigger("AnimTreeDie");
        }
    }
}
