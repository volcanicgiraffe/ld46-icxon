﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatSourceMB : MonoBehaviour
{
    public float fuel = 30f;
    public float fuelMax = 60f;
    public float fuelPerSecond = 1.0f;

    public float heatPerSecond = 1.0f;

    public Collider heatZone;
    public GameObject fireObject;
    public ParticleSystem sparks;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fuel > 0)
        {
            fuel -= Time.deltaTime * fuelPerSecond;

            float scale = 0.5f + (fuel / fuelMax) * 2;
            fireObject.transform.localScale = new Vector3(scale, scale, scale);

            if (fuel < 0)
            {
                fuel = 0;

                heatZone.enabled = false;
                fireObject.SetActive(false);
            }
        }
    }

    public void AddFuel(float amount)
    {
        if(amount > 0)
        {
            if (sparks.isPlaying) sparks.Stop();
            sparks.Play();
        }


        if (fuel == 0 && amount > 0)
        {
            heatZone.enabled = true;
            fireObject.SetActive(true);
        }

        fuel += amount;


        if (fuel > fuelMax) fuel = fuelMax;
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        HeatConsumerMB consumer = other.GetComponent<HeatConsumerMB>();

        if (consumer != null)
        {
            consumer.HeatUp(heatPerSecond);
        }
    }
}
