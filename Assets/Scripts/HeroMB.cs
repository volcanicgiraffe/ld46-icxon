﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class HeroMB : MonoBehaviour
{
    public Camera cam;
    public Transform frontSocket;
    public Transform backSocket;

    public ParticleSystem breath;
    public HeatConsumerMB myHeat;
    public Volume ppVolume;
    public OverlayMB overlayMB;

    public FancyTooltip tooltip;

    public FishtankItem fishtank;

    public AudioSource audio;

    private Vignette vignette;

    private float breathDelay = 3.5f;
    private float breathTimer = 3f;

    private ItemMB frontItem;
    private ItemMB backItem;

    private float speed = 4f;
    private float ropeSpeedFactor = 0.2f;
    private float freezeSpeedFactor = 1f;
    Rigidbody rb;
    int floorMask;
    float camRayLength = 100f;

    Vector3 movement;

    private List<ItemMB> items;

    private bool isOnRope = false;

    private bool finished = false;

    // TUTORIAL
    private int nextColdWarning = 0;
    private float[] coldWarnings = { 4.0f, 3.0f, 2.0f, 0.5f, 0.1f, 0f, -1000f };
    private string[] coldWarningTexts = { "Getting cold", "I'm freezing", "Can't feel my legs", "Ugh...", "I love you, my fish <3", "?", "???" };

    private int nextFishColdWarning = 0;
    private float[] fishColdWarnings = { 3.0f, 2.0f, 0.5f, 0.1f, 0f, -1000f };
    private string[] fishColdWarningTexts = { "My fish is sad", "My fish is freezing", "I need to keep it alive!", "Sorry, my little fish.", "?", "???" };

    private int creditsNext = 0;
    private float[] creditsTimings = { 3.0f, 7.0f, 15f, 19f, 23f, 60f, 100f, 10000f };
    private string[] creditsTexts = { "You are free now!", "My fish is free...", "Hey! Thanks for playing :)", "(c) Volcanic Giraffe", "Special for Ludum Dare 46", "You should go now :P", "Go play and rate other games!", "The END!" };
    private float creditsTimer = 0f;

    private float tutCooldown = 5f;

    private float tutCanClimbThis = 0f;

    private float tutMyLovelyFish = 0f;
    private bool tutHatchet = false;
    private float tutNoodleSoup = 0f;
    private float tutTorch = 0f;
    private float tutTorchEmpty = 0f;
    private float tutLog = 0f;
    private float tutUseTab = 0f;
    private float tutCampfire = 0f;
    private float tutNeedAxe = 0f;
    private float tutRopeCareful = 0f;

    private int deathCount = 0;

    private Vector3 originalPosition = Vector3.zero;
    private Quaternion originalRotation = Quaternion.identity;

    private Vector3 fishtankPosition = Vector3.zero;
    private Quaternion fishtankRotation = Quaternion.identity;

    private float wasdTimer = 5.0f;
    private bool dying = false;
    private float dieTimer = 0f;

    ////////////////////////////////////////////

    void Start()
    {

        originalPosition = transform.position;
        originalRotation = transform.rotation;

        fishtankPosition = fishtank.transform.position;
        fishtankRotation = fishtank.transform.rotation;

        floorMask = LayerMask.GetMask("Floor");
        rb = GetComponent<Rigidbody>();

        items = new List<ItemMB>();

        if (ppVolume != null)
        {
            ppVolume.profile.TryGet<Vignette>(out vignette);
        }

        ShowWithSound("What a weird dream...");

        overlayMB.FadeIn(0);
    }

    // Update is called once per frame
    void Update()
    {
        bool switchAction = Input.GetKeyDown(KeyCode.Tab) || Input.GetMouseButtonDown(1) || Input.GetButtonDown("Fire2");

        if (switchAction)
        {
            SwapItems();
        }

        if (breathTimer > 0 && !myHeat.IsUnderHeat())
        {
            breathTimer -= Time.deltaTime;

            if (breathTimer < 0)
            {
                breathTimer = breathDelay;
                if (breath.isPlaying) breath.Stop();
                breath.Play();
            }
        }

        if (vignette != null)
        {
            if(finished)
            {
                vignette.intensity.value = 0.5f;
            } else
            {
                float heat = myHeat.HeatNormalized();
                vignette.intensity.value = Mathf.Max(1f - heat, 0.2f);
            }   
        }

        HandleItemInteraction();


        TutorialHeatWarnings();
        TutorialFishHeatWarnings();
        UpdateCredits();

        if (myHeat.heat < 0.2f)
        {
            freezeSpeedFactor = myHeat.heat * 3f;
        }
        else if (myHeat.heat < 2.0f)
        {
            freezeSpeedFactor = 0.6f;
        } else
        {
            freezeSpeedFactor = 1f;
        }

        if (myHeat.heat <= 0f || fishtank.myHeat.heat <= 0f)
        {
            DieTragically();
        }

        

        if (wasdTimer > 0)
        {
            wasdTimer -= Time.deltaTime;
            if (wasdTimer <= 0)
            {
                if (deathCount == 0)
                {
                    ShowWithSound("WASD move, SPACE interact");
                } 
                else
                {
                    ShowWithSound("...");
                }

                
            }
        }

        if (dieTimer > 0)
        {
            dieTimer -= Time.deltaTime;

            if(dieTimer <= 0)
            {
                Reincarnate();
            }
        }


        tutCanClimbThis -= Time.deltaTime;
        tutMyLovelyFish -= Time.deltaTime;
        tutNoodleSoup -= Time.deltaTime;
        tutTorch -= Time.deltaTime;
        tutTorchEmpty -= Time.deltaTime;
        tutLog -= Time.deltaTime;
        tutUseTab -= Time.deltaTime;
        tutCampfire -= Time.deltaTime;
        tutNeedAxe -= Time.deltaTime;
        tutRopeCareful -= Time.deltaTime;
    }

    private void UpdateCredits()
    {
        if (!finished) return;

        creditsTimer += Time.deltaTime;

        if (creditsTimer > creditsTimings[creditsNext])
        {
            ShowWithSound(creditsTexts[creditsNext]);

            creditsNext += 1;
        }
    }

    private void DieTragically()
    {
        if (dying || finished) return;
        
        dying = true;
        dieTimer = 1.2f;

        overlayMB.FadeOut();
    }

    void ShowWithSound(string message)
    {
        audio.Play();
        tooltip.ShowText(message);
    }

    void Reincarnate()
    {
        deathCount++;
        dying = false;
        overlayMB.FadeIn(2);
        ShowWithSound("What a weird dream...");

        if (isOnRope)
        {
            DeattachFromRope(null);
        }

        wasdTimer = 5f;

        transform.position = originalPosition;
        transform.rotation = originalRotation;

        if (fishtank != frontItem && fishtank != backItem)
        {
            fishtank.transform.position = fishtankPosition;
            fishtank.transform.rotation = fishtankRotation;
        }
        fishtank.myHeat.ResetHeat();
        myHeat.ResetHeat();
    }

    private void TutorialHeatWarnings()
    {
        if (dying || finished) return;

        if (myHeat.heat > coldWarnings[0])
        {
            nextColdWarning = 0;
        }

        if (myHeat.heat < coldWarnings[nextColdWarning])
        {
            ShowWithSound(coldWarningTexts[nextColdWarning]);
            nextColdWarning++;
        }
    }
    private void TutorialFishHeatWarnings()
    {
        if (dying || finished) return;

        if (fishtank.myHeat.heat > fishColdWarnings[0])
        {
            nextFishColdWarning = 0;
        }

        if (fishtank.myHeat.heat < fishColdWarnings[nextFishColdWarning])
        {
            ShowWithSound(fishColdWarningTexts[nextFishColdWarning]);
            nextFishColdWarning++;
        }
    }

    private void TutorialItemGrab(ItemMB item)
    {
        if (dying || finished) return;


        if (item is PondItem && (frontItem == null || !(frontItem is FishtankItem)))
        {
            ShowWithSound("I need my fish here.");
            return;
        }

        if (frontItem != null && item.canGrab && tutUseTab <= 0)
        {
            if (backItem != null)
            {
                tutUseTab = tutCooldown;
                ShowWithSound("My hands are full!");
            } else
            {
                tutUseTab = tutCooldown;
                ShowWithSound("Press [TAB] to swap items.");
            }
            return;
        }

        if (frontItem != null && item is CampfireItem)
        {
            var fire = item.GetHeatSource();

            if (frontItem is FishtankItem || frontItem is HatchetItem)
            {
                ShowWithSound("I am not burning this!");
                return;
            }

            if (fire.fuel > 0.1f)
            {
                
            }
            else
            {
                if (frontItem is SnackItem)
                {
                    ShowWithSound("Need fire to cook this");
                }
            }
            
        }

        if ((frontItem == null || !(frontItem is HatchetItem)) && tutNeedAxe <= 0 && item is TreeItem)
        {
            if(((TreeItem)item).chopsToDie <= 0)
            {
                tutNeedAxe = tutCooldown;
                ShowWithSound("Nothing left here");
            }
            else
            {
                tutNeedAxe = tutCooldown;
                ShowWithSound("Can't use bare hands here.");
            }
        }


        if (frontItem == null && tutCampfire <= 0 && item is CampfireItem)
        {
            if (item.GetHeatSource().fuel < 0.1)
            {
                tutCampfire = tutCooldown;
                ShowWithSound("I need something flamable...");
            } else
            {
                tutTorch = 600f;
                ShowWithSound("That's hot!");
            }
        }
        if (frontItem == null && tutMyLovelyFish <= 0 && item is FishtankItem)
        {
            tutMyLovelyFish = 120f;

            if(deathCount == 0)
            {
                ShowWithSound("My lovely fish <3");
            } else
            {
                ShowWithSound("My lovely fish... again.");
            }
        }

        if (frontItem == null && !tutHatchet && item is HatchetItem)
        {
            tutHatchet = true;
            ShowWithSound("Hatchet!");
        }

        if (frontItem == null && tutNoodleSoup <= 0 && item is SnackItem)
        {
            tutNoodleSoup = 60f;
            if (deathCount == 0)
            {
                ShowWithSound("Chicken Noodle Soup!");
            }
            else
            {
                ShowWithSound("Noodle Soup... again.");
            }
           
        }

        if (frontItem == null && (tutTorch <= 0 || tutTorchEmpty <= 0) && item is TorchItem)
        {
            var torch = item.GetHeatSource();

            if (torch.fuel > 0.1f && tutTorch <= 0)
            {
                tutTorch = 600f;
                ShowWithSound("That's hot!");
            }
            if (torch.fuel <= 0.1f && tutTorchEmpty <= 0)
            {
                tutTorchEmpty = 120f;
                ShowWithSound("Burned torch");
            }
        }

        if (frontItem == null && tutLog <= 0 && item is LogItem)
        {
            tutLog = 600f;
            ShowWithSound("Firwood!");
        }

        
    }

    private void HandleItemInteraction()
    {
        if (dying || finished) return;

        bool action = Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1");


        if (action && items.Count > 0)
        {
            ItemMB item = items[0];

            TutorialItemGrab(item);

            if (item.canGrab && frontItem == null)
            {
                PutItemInHands(item);
                item.OnPlayerLeave();
            }
            else
            {
                item.OnInteract(this, frontItem);
            }

            return;
        }

        // DROP
        if (action && frontItem != null)
        {
            frontItem.gameObject.transform.SetParent(null);
            frontItem.OnDropped();

            frontItem = null;
            return;
        }
    }

    public void PutItemInHands(ItemMB item)
    {
        if (frontItem != null) return;

        frontItem = item;

        item.gameObject.transform.SetParent(frontSocket);
        item.OnTake();

        items.Remove(item);
        return;
    }

    public void SwapItems()
    {
        if (dying) return;

        if (isOnRope) return;

        if (frontItem == null && backItem == null) return;

        ItemMB oldFront = frontItem;
        ItemMB oldBack = backItem;

        frontItem = null;
        backItem = null;

        if (oldBack != null)
        {
            oldBack.gameObject.transform.SetParent(frontSocket);
            oldBack.gameObject.transform.localRotation = Quaternion.identity;
            oldBack.gameObject.transform.localPosition = Vector3.zero;

            frontItem = oldBack;
        }
        if (oldFront != null)
        {
            oldFront.gameObject.transform.SetParent(backSocket);
            oldFront.gameObject.transform.localRotation = Quaternion.identity;
            oldFront.gameObject.transform.localPosition = Vector3.zero;

            backItem = oldFront;
        }
    }

    public void DestroyItemInHands ()
    {
        if (frontItem != null)
        {
            Destroy(frontItem.gameObject);
            frontItem = null;
        }
    }

    public void SoupBoost()
    {
        ShowWithSound("Imune to cold for 10 seconds");

        myHeat.HeatBoost(10f);
    }

    private void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
    }

    void Move(float h, float v)
    {
        if (dying || finished) return;

        if (isOnRope)
        {
            movement.Set(0f, v, 0f);
            movement = movement.normalized * speed * Time.deltaTime * ropeSpeedFactor * freezeSpeedFactor;
            rb.MovePosition(transform.position + transform.rotation * movement);
        }
        else
        {
            movement.Set(h, 0f, v);
            movement = movement.normalized * speed * Time.deltaTime * freezeSpeedFactor;
            rb.MovePosition(transform.position + movement);
        }
    }

    void Turning()
    {
        if (dying || finished) return;

        if (isOnRope) return;

        Ray camRay = cam.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            rb.MoveRotation(newRotation);
        }
    }

    public void AttachToRope(RopeItem rope)
    {
        isOnRope = true;
        rb.isKinematic = true;
        transform.position = rope.attachPoint.position;
        transform.rotation = rope.transform.rotation;
    }

    public void DeattachFromRope(Transform exitPoint)
    {
        if (exitPoint != null)
        {
            transform.position = exitPoint.position;
            transform.rotation = Quaternion.identity;
        }

        rb.isKinematic = false;
        isOnRope = false;

        if (tutRopeCareful <= 0)
        {
            ShowWithSound("Careful!");
            tutRopeCareful = 600f;
        }
    }

    public void GameCompleted()
    {
        finished = true;
        myHeat.UnlimitedHeat();
    }

    private void OnTriggerEnter(Collider other)
    {

        ItemMB item = other.gameObject.GetComponent<ItemMB>();

        if (item != null)
        { 
            item.OnPlayerEnter();
            if (!items.Contains(item))
            {
                items.Add(item);
            }

            if (item is PondItem && !finished)
            {
                ShowWithSound("Whoah...");
            }


            if (tutCanClimbThis <= 0 && item is RopeItem)
            {
                if (frontItem != null)
                {
                    ShowWithSound("My hands are full!");
                }
                else
                {
                    tutCanClimbThis = tutCooldown;
                    if (deathCount == 0)
                    {
                        ShowWithSound("I can climb this!");
                    }
                    else
                    {
                        ShowWithSound("I can climb this... again.");
                    }
                }
                
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        ItemMB item = other.gameObject.GetComponent<ItemMB>();

        if (item != null)
        {
            if (items.Contains(item))
            {
                items.Remove(item);
            }
            item.OnPlayerLeave();
        }
    }
}
