﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeatConsumerMB : MonoBehaviour
{
    public float heat = 5f;
    public float heatMax = 10f;
    public float heatTransferSpeed = 1.0f;
    public float heatLossPerSecond = 0.1f;

    private float heatBoost = 1.0f;
    private float heatBoostTimer = 0f;

    private float heatedKD = 1f;
    private float heatedTimer = 1f;

    private float Oheat;
    private float OheatMax;
    private float OheatTransferSpeed;
    private float OheatLossPerSecond;

    private float OheatBoost;
    private float OheatBoostTimer;

    private float OheatedKD;
    private float OheatedTimer;


    private bool unlimited = false;

    // Start is called before the first frame update
    void Start()
    {
        Oheat = heat;
        OheatMax = heatMax;
        OheatTransferSpeed = heatTransferSpeed;
        OheatLossPerSecond = heatLossPerSecond;
        OheatBoost = heatBoost;
        OheatBoostTimer = heatBoostTimer;
        OheatedKD = heatedKD;
        OheatedTimer = heatedTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (unlimited) return;

        heat -= Time.deltaTime * heatLossPerSecond * heatBoost;

        if (heat < 0)
        {
            heat = 0;
        }

        if (heatedTimer > 0)
        {
            heatedTimer -= Time.deltaTime;
        }

        if(heatBoostTimer > 0)
        {
            heatBoostTimer -= Time.deltaTime;
            if (heatBoostTimer <= 0)
            {
                heatBoost = 1.0f;
            }
        }
    }

    public bool IsUnderHeat()
    {
        return heatedTimer >= 0;
    }

    public float HeatNormalized()
    {
        return heat / heatMax;
    }

    public void HeatUp(float heatPerSecond)
    {
        heatedTimer = heatedKD;

        heat += Time.deltaTime * heatPerSecond * heatTransferSpeed;

        if (heat > heatMax)
        {
            heat = heatMax;
        }
    }

    public void UnlimitedHeat()
    {
        heat = OheatMax;
        unlimited = true;
    }

    public void HeatBoost(float duration)
    {
        heat = OheatMax;
        heatBoost = 0.0f;
        heatBoostTimer = duration;
    }

    public void ResetHeat()
    {
        heat = Oheat;
        heatMax = OheatMax;
        heatTransferSpeed = OheatTransferSpeed;
        heatLossPerSecond = OheatLossPerSecond;
        heatBoost = OheatBoost;
        heatBoostTimer = OheatBoostTimer;
        heatedKD = OheatedKD;
        heatedTimer = OheatedTimer;
    }
}
