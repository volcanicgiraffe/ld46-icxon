﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroText : MonoBehaviour
{
    public Transform hero;
    public Camera cam;
    public Vector3 posOffset;

    // Start is called before the first frame update
    void Start()
    {
        posOffset = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = cam.WorldToScreenPoint(hero.position + posOffset);
    }
}
